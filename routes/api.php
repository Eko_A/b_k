<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
    Route::resource('forum', 'ForumController');
});

Route::post('/api/login', 'UserController@login')->name('login');
Route::post('/api/register', 'UserController@register')->name('register');

Route::group(['prefix'=> 'api', 'middleware' => 'auth:api'], function () {
    Route::resource('forum', 'ForumController');
    Route::resource('comment', 'CommentController');
    Route::resource('user', 'UserController');
});

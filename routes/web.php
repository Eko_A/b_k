
<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

//authentication
Route::post('/api/login', 'UserController@login')->name('login');
Route::post('/api/register', 'UserController@register')->name('register');
Route::post('/api/passwordreset', 'UserController@passwordreset')->name('password-reset');
Route::get('/resetpassword/{hash}', 'UserController@resetpassword')->name('reset-password');

//external links
Route::post('/password/reset/', [
	'uses' => 'UserController@updatepassword',
	'as' => 'password.reset'
]);

Route::post('/invitation/position/', [
	'uses' => 'SmeController@adduser',
	'as' => 'invitation.position'
]);

//for admin console
    Route::post('/api/resource/store', 'ResorceController@store')->name('resource-store');
    Route::post('/api/newsroom/store', 'NewsroomController@store')->name('news-store');
    Route::post('/api/newsroom/update/{id}', 'NewsroomController@update')->name('news-update');
    
    
//user
    Route::post('/api/user/changepassword', 'UserController@changepassword')->name('change-password');

//forum
//Route::resource('/forum', 'ForumController');

Route::group(['prefix'=> 'api', 'middleware' => 'auth:api'], function () {
    //forum
    Route::post('/forum/destroy/{id}', 'ForumController@destroy')->name('forum-destroy');
    Route::get('/forum/show/{id}', 'ForumController@show')->name('forum-show');
    Route::get('/forum/index', 'ForumController@index')->name('forum-index');
    Route::get('/forum/getcategories', 'ForumController@getcategories')->name('get-categories');
    Route::post('/forum/store', 'ForumController@store')->name('forum-store');
    Route::post('/forum/update/{id}', 'ForumController@update')->name('forum-update');
    Route::resource('forum', 'ForumController');

    //newsroom
    Route::get('/newsroom/index', 'NewsroomController@index')->name('newsroom-index');
    Route::get('/newsroom/show/{id}', 'NewsroomController@show')->name('newsroom-show');
    Route::post('/newsroom/addbookmark', 'NewsroomController@addbookmark')->name('add-bookmark');
    Route::post('/newsroom/removebookmark', 'NewsroomController@removebookmark')->name('remove-bookmark');
    Route::get('/newsroom/viewbookmarks', 'NewsroomController@viewbookmarks')->name('view-bookmarks');
    Route::resource('newsroom', 'NewsroomController');

    //user
    Route::get('/user/getinvestors', 'UserController@getinvestors')->name('get-getinvestors');
    Route::get('/user/entrepreneurprofile/{id}', 'UserController@entrepreneurprofile')->name('get-entrepreneurprofile');
    Route::get('/user/investorprofile/{id}', 'UserController@investorprofile')->name('get-investorprofile');
    Route::get('/user/getuser', 'UserController@getuser')->name('get-user');
    Route::post('/user/discover', 'UserController@discover')->name('discover');
    Route::post('/user/setdeviceid', 'UserController@setdeviceid')->name('setdeviceid');
    Route::post('/user/unsetdeviceid', 'UserController@unsetdeviceid')->name('unsetdeviceid');
    Route::post('/user/update/{id}', 'UserController@update')->name('user-update');
    //Route::post('/user/changepassword/{id}', 'UserController@changepassword')->name('change-password');
    Route::resource('/user', 'UserController');

    //comments
    Route::post('/comment/update/{id}', 'CommentController@update')->name('comment-update');
    Route::post('/comment/destroy/{id}', 'CommentController@destroy')->name('comment-destroy');
    Route::post('/comment/store', 'CommentController@store')->name('comment-store');
    Route::resource('/comment', 'CommentController');

    //upvote
    Route::post('/upvote', 'UpvoteController@upvote')->name('upvote');
    Route::get('/downvote/{id}', 'UpvoteController@downvote')->name('downvote');

    //sme
    Route::get('/sme/smestages', 'SmeController@smestages')->name('get-smestages');
    Route::get('/sme/smeprofile/{id}', 'UserController@smeprofile')->name('get-smeprofile');
    Route::get('/sme/getsmebyindustry/{id}', 'SmeController@getsmebyindustry')->name('get-sme-by-industry');
    Route::get('/sme/getsme', 'SmeController@getsme')->name('get-sme');
    Route::post('/sme/followinvestor', 'SmeController@followinvestor')->name('follow-investor');
    Route::post('/sme/deletemember', 'SmeController@deletemember')->name('delete-member');
    Route::post('/sme/editposition', 'SmeController@editposition')->name('edit-position');
    Route::post('/sme/store', 'SmeController@store')->name('sme-store');
    Route::post('/sme/update/{id}', 'SmeController@update')->name('sme-update');
    Route::get('/sme/categories', 'SmeController@categories')->name('sme-categories');
    Route::get('/sme/findcategory', 'SmeController@findcategory')->name('sme-findcategory');
    Route::resource('sme', 'SmeController');

    //resources
    Route::get('/resource/show/{id}', 'ResorceController@show')->name('resource-show');
    Route::get('/resource/index', 'ResorceController@index')->name('resource-index');
    Route::post('/resource/update/{id}', 'ResorceController@update')->name('resource-update');
    Route::resource('resource', 'ResorceController');


    //badges
    Route::post('/badges/updatebadge/{badge_id}', 'BadgeController@updatebadge')->name('updatebadge');
    Route::get('/badges/getbadgesbyid/{badge_id}/{sme_id}', 'BadgeController@getbadgesbyid')->name('getbadgesbyid');
    Route::get('/badges/getbadgesbyinvestorid/{investor_id}', 'BadgeController@getbadgesbyinvestorid')->name('getbadgesbyinvestorid');
    Route::get('/badges/show/{badge_id}/{investor_id}', 'BadgeController@show')->name('badges-show');
    Route::get('/badges/getbadges', 'BadgeController@getbadges')->name('get-badges');
    Route::post('/badge/update/{id}', 'BadgeController@update')->name('badge-update');
    Route::resource('badges', 'BadgeController');
});
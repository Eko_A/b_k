<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;


class Forum extends Model
{
    //
    protected $primaryKey = 'forum_id';
    protected $fillable = ['text', 'posted_by'];
    protected $hidden = ['posted_by'];
    //to check if the user has liked a post
    protected $appends = ['liked_by_auth_user'];
    
    
    public function user()
    {
        return $this->belongsTo('App\User','user_id','id');
    }
    
    public function upvotes()
    {
        return $this->hasMany('App\Upvote','forum_id','forum_id');
        
    }

    public function getLikedByAuthUserAttribute()
    {
            $userId = Auth::guard('api')->id();
            
            $upvote = $this->upvotes->where('user_id', $userId)->first(function ($key, $value) use ($userId) {
                //dd($key);
                //return $key->user_id === $userId;
                //dd($key->user_id);
                //return $key->user_id;
                return $key;
            });
            
           // return $upvote;
            /*if(is_array($upvote))
            {
                dd($upvote);
            }*/

            if ($upvote) {
                return true;
                /*foreach($upvote as $value)
                {
                    //dd($value);
                    if($value->user_id == $userId)
                    {
                        return true;
                    }
                }*/
            }
            else{
                return false;
            }
    }
}

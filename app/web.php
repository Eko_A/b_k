<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home')->middleware(['cors']);


//authentication
Route::post('/api/login', 'UserController@login')->name('login')->middleware(['cors']);

Route::post('/api/register', 'UserController@register')->name('register')->middleware(['cors']);


//forum

//Route::resource('/forum', 'ForumController');

Route::group(['prefix'=> 'api', 'middleware' => ['auth:api', 'cors']], function () {
    //forum
    Route::post('/forum/destroy/{id}', 'ForumController@destroy')->name('forum-destroy');
    Route::get('/forum/show/{id}', 'ForumController@show')->name('forum-show');
    Route::get('/forum/index', 'ForumController@index')->name('forum-index');
    Route::get('/forum/getcategories', 'ForumController@getcategories')->name('get-categories');
    Route::post('/forum/store', 'ForumController@store')->name('forum-store');
    Route::post('/forum/update/{id}', 'ForumController@update')->name('forum-update');
    Route::resource('forum', 'ForumController');

    //newsroom
    Route::get('/newsroom/index', 'NewsroomController@index')->name('newsroom-index');
    Route::get('/newsroom/show/{id}', 'NewsroomController@show')->name('newsroom-show');
    Route::resource('newsroom', 'NewsroomController');

    //user
    Route::get('/user/getuser', 'UserController@getuser')->name('get-user');
    Route::post('/user/discover', 'UserController@discover')->name('discover');
    Route::resource('user', 'UserController');

    //comments
    Route::post('/comment/store', 'CommentController@store')->name('comment-store');
    Route::resource('comment', 'CommentController');

    //sme
    Route::get('/user/getsme', 'SmeController@getsme')->name('get-sme');
    Route::post('/sme/store', 'SmeController@store')->name('sme-store');
    Route::get('/sme/categories', 'SmeController@categories')->name('sme-categories');
    Route::get('/sme/findcategory', 'SmeController@findcategory')->name('sme-findcategory');
    Route::resource('sme', 'SmeController');

});
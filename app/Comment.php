<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    //
    protected $fillable = ['text', 'user_id', 'post_id'];
    //protected $fillable = ['text', 'post_id'];
    protected $hidden = ['user_id', 'post_id'];
}

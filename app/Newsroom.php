<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Newsroom extends Model
{
    //
    //protected $table = 'newss';
    //to check if the user has liked a post
    protected $appends = ['liked_by_auth_user'];
    
    
    public function user()
    {
        return $this->belongsTo('App\User','user_id','id');
    }
    
    public function bookmarks()
    {
       $userId = Auth::guard('api')->id();
       return $this->hasMany('App\Bookmark','news_id','id')->select('user_id')->where('user_id', $userId);
    }
    
    public function getLikedByAuthUserAttribute()
    {
            $userId = Auth::guard('api')->id();
            
            $bookmark = $this->bookmarks->first(function ($key, $value) use ($userId) {
                return $key;
            });

            if ($bookmark) {
                return true;
                /*foreach($upvote as $value)
                {
                    //dd($value);
                    if($value->user_id == $userId)
                    {
                        return true;
                    }
                }*/
            }
            else{
                return false;
            }
    }
}

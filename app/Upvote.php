<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Upvote extends Model
{
    //
    public function forum()
    {
        return $this->belongsTo('App\Forum','forum_id','id');
    }

    public function user()
    {
        return $this->belongsTo('App\User','user_id','id');
    }
}

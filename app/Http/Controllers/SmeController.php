<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Sme;
use App\Smecategory;
use App\User;
use App\Badge;
use App\Badgecategory;
use Storage;
use DB;

class SmeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }
    
    public function getsmebyindustry($id)
    {
       if(isset($id))
       {
           $industry = array($id);
           $smes = Sme::select('id','profile_pic','business_name', 'short_bio', 'industry', 'user_id','badge_id')->whereRaw("find_in_set($id,industry)")->get();
           
           /*$smes = DB::table('smes')
                   ->join('badge_categories', 'smes.badge_id', '=', 'badge_categories.id')
                   ->select('smes.id','smes.profile_pic','smes.business_name', 'smes.short_bio', 'smes.industry', 'smes.user_id','smes.badge_id', 'badge_categories.badge_name')
                   
                   ->where('smes.badge_id', 'badge_categories.id')
                   ->where('smes.industry', $id)
                   //->whereRaw("find_in_set($id,smes.industry)")
                   ->get();*/

           if(count($smes)>0)
           {
            $status = TRUE;
            return response()->json(['status'=>$status, 'smes'=>$smes]);
           }
           else{
               $status = TRUE;
               return response()->json(['status'=>$status, 'smes'=>$smes]);
               /*$msg = "No SMEs in this industry yet";
               return response()->json(['status'=>$status, 'msg'=>$msg]);*/
           }
       }
       else{
               $status = FALSE;
               $msg = "No ID provided";
               return response()->json(['status'=>$status, 'msg'=>$msg]);
       } 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        if(!empty($request->business_name) && !empty($request->short_bio) && !empty($request->industry) && !empty($request->user_id)){
            $sme = new Sme;
            if(empty($request->profile_pic)){
            $sme->profile_pic = 'default.jpg';
            }else{
                $imagename = time().'.'.$request->profile_pic->getClientOriginalExtension();
                $request->profile_pic->move(public_path('storage/sme_pic'), $imagename);
                $sme->profile_pic = $imagename;
            }
            $sme->business_name = $request->business_name;
            $sme->short_bio = $request->short_bio;
            $sme->industry = $request->industry;
            $sme->user_id = $request->user_id;
            $sme->stage = $request->stage;
            $sme->location = $request->location;
            $sme->year_founded = $request->year_founded;
            $sme->no_of_employees = $request->no_of_employees;
            $sme->requirement = $request->requirement;
            if(isset($request->website))
            {
                $sme->website = $request->website;
            }
            if(isset($request->pitch_deck))
            {
                $imagename = time().'.'.$request->pitch_deck->getClientOriginalExtension();
                $request->pitch_deck->move(public_path('storage/pitch_deck'), $imagename);
                $sme->pitch_deck = $imagename;
            }
            
            $sme->save();
            
            $user = User::find($request->user_id);
            $user->sme_id = $sme->id;
            $user->save();

            if(!empty($request->email)){
            $this->inviteuser($sme->id, $request->email);
            }
            
            $status=TRUE;
            $sme = Sme::select('id','profile_pic', 'business_name', 'short_bio', 'industry', 'user_id', 'badge_id')->where('id', $sme->id)->first();
            return response()->json(['status'=>$status, 'sme'=>$sme]);
        }
        else{
            if(empty($request->business_name))
            {
                $msg="Please enter a buisness name";
            }
            if(empty($request->short_bio))
            {
                $msg="Please enter a description of your buisness";
            }
            if(empty($request->industry))
            {
                $msg="Please select an industry";
            }
            
            if(empty($request->user_id))
            {
                $msg="Please provide user id";
            }

            if(!empty($msg))
            {
                $status = FALSE;
                return response()->json(['status'=>$status, 'msg'=>$msg]);
            }
        }

    }
    
    public function smestages()
    {
        $sme_stages = DB::table('startup_stages')->select('stage')->get();
        
        if(count($sme_stages)>0)
        {
            $status = TRUE;
            return response()->json(['status'=>$status, 'sme_stages'=>$sme_stages]);
        }
        else
        {
            $status = FALSE;
            $msg = "No SME categories found";
            return response()->json(['status'=>$status, 'msg'=>$msg]);
        }
    }

    public function categories()
    {
        //total category count
        $category_count = Smecategory::select('categories_id')->count();
        //get all smes
        $all_smes = Sme::select('id', 'badge_id', 'industry')->orderBy('badge_id', 'desc')->get();
        //get all badges
        $all_badges = Badgecategory::select('id')->orderBy('id', 'desc')->get();
        
        if(empty($category_count) || empty($all_smes) || empty($all_badges))
        {
            $sme_categories = Smecategory::select('categories_id', 'categories_image', 'categories_name')->get();
            
            if(count($sme_categories)>0)
            {
                $status = TRUE;
                return response()->json(['status'=>$status, 'sme_categories'=>$sme_categories]);
            }
            else{
                $status = FALSE;
                $msg = "No sme categories have been added";
                return response()->json(['status'=>$status, 'msg'=>$msg]);
            }
        }
        
        $unique_industries = [];
        
        //get distinct industries from highest badge to lowest
        foreach($all_badges as $v) {
            foreach($all_smes as $value)
            {
                    if (empty($unique_industries) || !in_array($value->industry, $unique_industries, TRUE)) {
                        $unique_industries[] = $value->industry;
                    }
            }
        }
        
        foreach($unique_industries as $val)
        {
            //get details of industries in the unique insustries stred previously
            $sme_cat[] = Smecategory::select('categories_id', 'categories_image', 'categories_name')->where('categories_id', $val)->first();
        }

        if(count($unique_industries) < $category_count){
            
            foreach($sme_cat as $valu)
            {
                $sel_sme[] = $valu->categories_id;
            }
            //dd($sel_sme); 
            
            $sme_categ = Smecategory::select('categories_id', 'categories_image', 'categories_name')->WhereNotIn('categories_id', $sel_sme)->get()->toArray();

            //$sme_categ = array_diff_assoc($sme_cat, $sme_cate);
            $sme_categories = array_merge($sme_cat, $sme_categ);
            //$sme_categories = array_unique($sme_categories);
        }
        else
        {
            $sme_categories = Smecategory::select('categories_id', 'categories_image', 'categories_name')->get();
        }
        
       /* DB::table('smes')
                   ->join('badge_categories', 'smes.badge_id', '=', 'badge_categories.id')*/
        
        $smes = Sme::join('badge_categories', 'smes.badge_id', '=', 'badge_categories.id')->select('smes.id', 'smes.business_name', 'smes.profile_pic', 'smes.badge_id', 'smes.industry', 'badge_categories.badge_name')->orderBy('badge_id', 'desc')->limit(10)->get();
        
        if(count($sme_categories)>0 && count($smes)>0)
        {
            $status = TRUE;
            return response()->json(['status'=>$status, 'sme_categories'=>$sme_categories, 'smes'=>$smes]);
        }
        else if(count($sme_categories)>0){
            $status = TRUE;
            return response()->json(['status'=>$status, 'sme_categories'=>$sme_categories]);
        }
        else{
            $status = FALSE;
            $msg = "No sme categories have been added";
            return response()->json(['status'=>$status, 'msg'=>$msg]);
        }
    }

    public function findcategory(Request $request)
    {
       if(count($request->categories)>0)
       { 
        $categories = $request->categories;
       }
       else{
            $status = FALSE;
            $msg = "Category id is missing";
            return response()->json(['status'=>$status, 'msg'=>$msg]);
       }

       if(is_array($categories))
       {
        //check if it is an array
        //dd($categories);
            foreach($categories as $key => $value){
                $category = Smecategory::select('categories_name')->where('categories_id', $value)->first();
                $sme_category[] = $category;
                }
       }
       else{
            $sme_category = Smecategory::select('categories_name')->where('categories_id', $categories)->first();
       }

       if(count($sme_category)>0)
       {
            $status = TRUE;
            return response()->json(['status'=>$status, 'sme_category'=>$sme_category]);
       }
       else{
            $status = FALSE;
            $msg = "No category found";
            return response()->json(['status'=>$status, 'msg'=>$msg]);
       }
    }

    //sends invitation email
    public function inviteuser($sme_id, $email)
    {
        if(!empty($sme_id) && !empty($email))
        {
            $sme = SME::select('id', 'business_name')->where('id', $sme_id)->first();
            
            if(count($sme)>0)
            {
                if(is_array($email))
                {
                    foreach($email as $key => $value){
                        
                        $checkuser = User::select('id','first_name')->where('email', $value)->first();
                            
                        if(count($checkuser)<1)
                        {
                            $status = FALSE;
                            $msg = "This user $value doesn't exist";
                            return response()->json(['status'=>$status, 'msg'=>$msg]);
                        }
                        
                            DB::table('sme_invites')->insert(
                                array('user_id' => "$checkuser->id", 'sme_id'=>"$sme->id")
                            );
                            
                        $inviteid = DB::table('sme_invites')->select('id')->latest()->first();
                        
                        $link =  "https://ekoadetolani.com/black_knights/invitation/GH39IJ393UN3440NCNKE8740JJHEMQGH39IJ393UN3440NCNKE8740JJHEMQ" . $inviteid;
                        
                        try{
                            Mail::send('emails.invitation', ['sme_name' => $sme->business_name, 'link'=>$link], function ($message) use($value) {
                        
                            $message->from('do-not-reply@black-knights.net', 'Black Knights');
                        
                            $message->to($value)->subject('Black Knights Invitation');
                        
                            });
                        }
                        catch(\Exception $e){
                            
                        }
                    }
                }
                else
                {
                    
                    $checkuser = User::where('email', $email)->first();
                    
                    if(count($checkuser)<1)
                    {
                        $status = FALSE;
                        $msg = "This user $email doesn't exist";
                        return response()->json(['status'=>$status, 'msg'=>$msg]);
                    }
                    
                    try{
                        Mail::send('emails.invitation', function ($message) use($email) {
                    
                        $message->from('do-not-reply@black-knights.net', 'Black Knights');
                    
                        $message->to($email)->subject('Invitation');
                    
                        });
                    }
                    catch(\Exception $e){
                        $status = TRUE;
                        $msg = "Whoop! An invitation has been sent";
                        return response()->json(['status'=>$status, 'msg'=>$msg]);
                    }
                }
                
                /*$status = TRUE;
                $msg = "Whoop! An invitation has been sent";
                reurn response()->json(['status'=>$status, 'msg'=>$msg]);*/
            }
            else
            {
                $msg = "SME not found";
            }
        }
        else
        {
            if(empty($request->sme_id))
            {
                $msg = "Please provide a valid SME id";
            }
            
            if(empty($request->email))
            {
                $msg = "Please provide a valid email address";
            }
        }
        
        if(!empty($msg))
        {
            $status = FALSE;
            return response()->json(['status'=>$status, 'msg'=>$msg]);
        }
    }
    
    //displays page
    public function invitation()
    {
        return view('blackknights.invitation');
    }
    
    //updates user account
    public function adduser(Request $request)
    {
        if(empty($request->link))
        {
            return "This link has expired";
        }
        $id = str_replace("/black_knights/invitation/GH39IJ393UN3440NCNKE8740JJHEMQGH39IJ393UN3440NCNKE8740JJHEMQ", "", "$request->link");
        
        $invite = DB::select('id', 'user_id', 'sme_id')->where('id', $id)->first();
        
        $checkuser = User::select('id', 'based')->where('id', $invite->user_id)->first();
        
        if(count($checkuser)>0)
        {
            $user = User::find($invite->user_id);
            $user->sme_id =  $invite->sme_id;
            $user->position = $request->position;
            $user->save();
            
            return "Successful!";
        }
        else
        {
            return "Invalid link";
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }
    
    public function followinvestor(Request $request)
    {
        if(empty($request->sme_id))
        {
            $msg = "Please provide an SME id";
        }
        
        if(empty($request->investor_id))
        {
            $msg = "Please provide an investor id";
        }
        
        if(isset($msg))
        {
            $status = FALSE;
            return response()->json(['status'=>$status, 'msg'=>$msg]);
        }
        else
        {
            $sme = SME::find($request->sme_id);
            $sme->investor_id = $request->investor_id;
            $sme->save();
            
            $status = TRUE;
            $msg = "Successful";
            return response()->json(['status'=>$status, 'msg'=>$msg]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    public function getsme($id)
    {
        if(!empty($id)){
        $sme = Sme::select('profile_pic', 'business_name', 'short_bio', 'industry', 'user_id', 'badge_id')->where('id', $id)->first();
        }
        else{
            $status = FALSE;
            $msg = "Id not available";
            return response()->json(['status'=>$status, 'msg'=>$msg]);   
        }

        if(!empty($sme))
        {
            $status = TRUE;
            return response()->json(['status'=>$status, 'sme'=>$sme]);
        }
        else{
            $status = FALSE;
            $msg = "SME not found";
            return response()->json(['status'=>$status, 'msg'=>$msg]); 
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
         if(!empty($id)){
            $sme = Sme::find($id);
            if(!empty($request->profile_pic)){
                $imagename = time().'.'.$request->profile_pic->getClientOriginalExtension();
                $request->profile_pic->move(public_path('storage/sme_pic'), $imagename);
                $sme->profile_pic = $imagename;
            }
            if(!empty($request->business_name)){
            $sme->business_name = $request->business_name;
            }
            if(!empty($request->short_bio)){
            $sme->short_bio = $request->short_bio;
            }
            if(!empty($request->industry)){
            $sme->industry = $request->industry;
            }
            if(!empty($request->stage)){
            $sme->stage = $request->stage;
            }
            if(!empty($request->location)){
            $sme->location = $request->location;
            }
            if(!empty($request->year_founded)){
            $sme->year_founded = $request->year_founded;
            }
            if(!empty($request->no_of_employees)){
            $sme->no_of_employees = $request->no_of_employees;
            }
            if(!empty($request->requirement)){
            $sme->requirement = $request->requirement;
            }
            if(!empty($request->website))
            {
                $sme->website = $request->website;
            }
            if(isset($request->pitch_deck))
            {
                $imagename = time().'.'.$request->pitch_deck->getClientOriginalExtension();
                $request->pitch_deck->move(public_path('storage/pitch_deck'), $imagename);
                $sme->pitch_deck = $imagename;
            }
            
            $sme->save();

            $status=TRUE;
            $sme = Sme::select('id','profile_pic', 'business_name', 'short_bio', 'industry', 'user_id', 'badge_id')->where('id', $sme->id)->first();
            return response()->json(['status'=>$status, 'sme'=>$sme]);
        }
        else{
                $status = FALSE;
                $msg = "No sme id provided";
                return response()->json(['status'=>$status, 'msg'=>$msg]);
            }
        
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    
    public function deletemember(Request $request)
    {
        $user_id = $request->user_id;
        $sme_id = $request->sme_id;
        
        if(empty($user_id))
        {
            $msg = "Please provide a user_id";
        }
        
        if(empty($sme_id))
        {
            $msg = "Please provide an sme id";
        }
        
        $checksme = Sme::where('id', $sme_id)->first();
        $checkuser = User::where('id', $user_id)->first();
        
        if(count($checksme)<1)
        {
            $msg = "No SME found";
        }
        
        if(count($checkuser)<1)
        {
            $msg = "User not found";
        }
        
        if(!empty($msg))
        {
            $status = FALSE;
            return response()->json(['status'=>$status, 'msg'=>$msg]);
        }
        else
        {
            $user = User::find($user_id);
            $user->sme_id = null;
            $user->save();
            
            $status = TRUE;
            $msg = "Successfully removed user";
            
            return response()->json(['status'=>$status, 'msg'=>$msg]);
        }
    }
    
    public function editposition(Request $request)
    {
        if(empty($request->user_id))
        {
            $msg = "Please provide User ID";
        }
        
        if(empty($request->position))
        {
            $msg = "Please provide a position";
        }
        
        if(!empty($msg))
        {
            $status = FALSE;
            return response()->json(['status'=>$status, 'msg'=>$msg]);
        }
        else
        {
            $user = User::find($request->user_id);
            $user->position = $request->position;
            $user->save();
            
            $status = TRUE;
            $msg = "Successfully updated position";
            return response()->json(['status'=>$status, 'msg'=>$msg]);
        }
    }
}

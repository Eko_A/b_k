<?php

namespace App\Http\Controllers;

use App\Newsroom;
use App\Bookmark;
use Storage;
use Auth;
use Illuminate\Http\Request;

class NewsroomController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $news = Newsroom::join('forum_category', 'newsrooms.category_id', '=', 'forum_category.fcategory_id')
            ->select('newsrooms.id','newsrooms.news_image', 'newsrooms.news_title', 'newsrooms.created_at')
            ->where('status', 'Active')
            ->orderBy('created_at', 'desc')
            ->get();
        if(count($news)>0){
            $status = TRUE;
            return response()->json(['status'=>$status, 'news'=>$news]);
        }
        else{
            $status = FALSE;
            $msg = "No news stories have been added yet";
            return response()->json(['status'=>$status, 'msg'=>$msg]);
        }
    }

//push notification function
    public function sendMessage($data,$target)
    {
    /*  
Parameter Example
	$data = array('post_id'=>'12345','post_title'=>'A Blog post');
	$target = 'single tocken id or topic name';
	or
	$target = array('token1','token2','...'); // up to 1000 in one request
*/    
        
    //FCM api URL
    $url = 'https://fcm.googleapis.com/fcm/send';
    //api_key available in Firebase Console -> Project Settings -> CLOUD MESSAGING -> Server key
    $server_key = 'AAAAaTqVlwo:APA91bHpyQSWZourOuKXavpFnoSNOcfUHxxAHzy-bdGj85GUpdsjgJTWuR6Tk4u1Z6Z1p0mptBj_CIxdKkJInsKu0e2Rlo7lrBHii2f67lJ6JiCoMbU-Q-rbnrG3qslmNJRYn2DEPUv3';
    
    //return $server_key;
    			
    $fields = array();
    //$fields['data'] = $data;
    $notif = array();
    $notif['title'] = "New Story";
    $notif['body'] = $data;
    $notif['content_available'] = true;
    $notif['priority'] = "high";
    $fields['notification'] = $notif;
    
    if(is_array($target)){
    	$fields['registration_ids'] = $target;
    }else{
    	$fields['to'] = $target;
    }
    
    //header with content_type api key
    $headers = array(
    	'Content-Type:application/json',
      'Authorization:key='.$server_key
    );
    			
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
    $result = curl_exec($ch);
    /*if ($result === FALSE) {
    	die('FCM Send Error: ' . curl_error($ch));
    }*/
    curl_close($ch);
    return $result;
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        if(empty($request->news_image))
        {
            $msg = "Please provide an image";
        }
        
        if(empty($request->news_title))
        {
            $msg = "Please provide a title";
        }
        
        if(empty($request->news_description))
        {
            $msg = "Please provide news content";
        }
        
        if(empty($request->category_id))
        {
            $msg = "Please provide category id";
        }
        
        if(empty($msg))
        {
            $news = new Newsroom;

               $imagename = time().'.'.$request->news_image->getClientOriginalExtension();
               $request->news_image->move(public_path('storage/news'), $imagename);
            
            $news->news_image = $imagename;
            if(!empty($request->news_title)){
            $news->news_title = $request->news_title;
            }
            if(!empty($request->news_description)){
            $news->news_description = $request->news_description;
            }
            
            $news->category_id = $request->category_id;
            
            $news->save();
            
            $data = $request->news_title;
	        $target = '/topics/general_information';
            $res = $this->sendMessage($data, $target);
            if($res === FALSE)
            {
                            $status = TRUE;
            $msg = "Successful";
            return response()->json(['status'=>$status, 'msg'=>$msg]);
            }
            
            //dd($res);
            
            $status = TRUE;
            $msg = "Successful";
            return response()->json(['status'=>$status, 'msg'=>$msg]);
        }
        else
        {
            $status = FALSE;
            return response()->json(['status'=>$status, 'msg'=>$msg]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {        //
        if(!empty($id))
        {
           $news = Newsroom::join('forum_category', 'newsrooms.category_id', '=', 'forum_category.fcategory_id')
           ->select('id','news_image', 'news_title', 'news_description', 'newsrooms.created_at','forum_category.fcategory_name')->where('id', $id)->first();
           /*$link = Storage::url('news/'. $news->news_image);
           return response()->json($link);*/ 

           if(count($news)>0)
           {
               $status = TRUE;
               return response()->json(['status'=>$status, 'news'=>$news]);
           }
           else{
               $status =FALSE;
               $msg = "News story not available";
               return response()->json(['status'=>$status, 'msg'=>$msg]);
           }         
        }
        else{
               $status =FALSE;
               $msg = "News not available";
        }

        if(!empty($msg)){
            $status = FALSE;
            return response()->json(['status'=>$status, 'msg'=>$msg]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\  $id
     * @return \Illuminate\Http\Response
     */
    public function edit( $id)
    {
        //
    }
    
    public function addbookmark(Request $request)
    {
        $user_id = Auth::guard('api')->id();
        if(empty($request->news_id))
        {
            $status = FALSE;
            $msg = "Please provide the news id";
            return response()->json(['status'=>$status, 'msg'=>$msg]);   
        }
        else
        {
            $bookmark = new Bookmark; 
            $bookmark->news_id = $request->news_id;
            $bookmark->user_id = $user_id;
            $bookmark->save();
            
            $status = TRUE;
            $msg = "Successfully added to bookmarks";
            return response()->json(['status'=>$status, 'msg'=>$msg]);  
        }
    }

    public function removebookmark(Request $request)
    {
        $user_id = Auth::guard('api')->id();
        if(empty($request->bookmark_id))
        {
            $status = FALSE;
            $msg = "Please provide the bookmark id";
            return response()->json(['status'=>$status, 'msg'=>$msg]);   
        }
        else
        {
            Bookmark::destroy($request->bookmark_id); 
            
            $status = TRUE;
            $msg = "Successfully removed from bookmarks";
            return response()->json(['status'=>$status, 'msg'=>$msg]);  
        }
    }
    
    public function viewbookmarks()
    {
        $user_id = Auth::guard('api')->id();
        $bookmarks = Bookmark::join('newsrooms', 'bookmarks.news_id', '=', 'newsrooms.id')
        ->select('bookmarks.id AS bookmark_id', 'bookmarks.user_id', 'newsrooms.id', 'newsrooms.news_image', 'newsrooms.news_title', 'news_description', 'newsrooms.created_at')
        ->where('bookmarks.user_id', $user_id)
        ->get();
        
        $status = TRUE;
        return response()->json(['status'=>$status, 'bookmarks'=>$bookmarks]);

    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,  $id)
    {
        //
        if(!empty($id))
        {
            $news = Newsroom::find($id);
            
            if(!empty($request->news_image))
            {
                $imagename = time().'.'.$request->news_image->getClientOriginalExtension();
                $request->news_image->move(public_path('storage/news'), $imagename);
               
                $news->news_image = $imagename;
            }
            
            if(!empty($request->news_title))
            {
                $news->news_title = $request->news_title;
            }
            
            if(!empty($request->news_description))
            {
                $news->news_description = $request->news_description;
            }
            
            if(!empty($request->category_id))
            {
            $news->category_id = $request->category_id;
            }
            
            $news->save();
            
                $status = TRUE;
                $msg = "Successful";
                return response()->json(['status'=>$status, 'msg'=>$msg]);
        }
        else
        {
            $status = FALSE;
            $msg = "Please provide the news id";
            return response()->json(['status'=>$status, 'msg'=>$msg]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy( $id)
    {
        //
        Newsroom::destroy($id);
        
        $status = TRUE;
        $msg = "Successfully deleted post";
        
        return response()->json(['status'=>$status, 'msg'=>$msg]);
    }
}

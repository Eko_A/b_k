<?php

namespace App\Http\Controllers;

use App\Forum;
use App\Forumcategory;
use App\Comment;
use App\Upvote;
use Illuminate\Http\Request;
use Auth;
use DB;

class ForumController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

/* $query = Author::query();
 
$query->when(request('filter_by') == 'likes', function ($q) {
    return $q->where('likes', '>', request('likes_amount', 0));
});
$query->when(request('filter_by') == 'date', function ($q) {
    return $q->orderBy('created_at', request('ordering_rule', 'desc'));
});
 
$authors = $query->get();

        $user_id = Auth::guard('api')->id();            
        $checkupvotes =  Upvote::Where('forums.forum_id', $id)->where('forums.user_id', $user_id)->get();  

        if(count($checkupvotes)>0){ 
            $upvoted = TRUE;
        }else{
            $upvoted = FALSE;
        }*/
        
        $forum = Forum::
                 //join('upvotes', 'forums.forum_id', '=', 'upvotes.forum_id')
                 //->join('comments', 'forums.forum_id', '=', 'comments.forum_id')
                 join('users', 'forums.posted_by', '=', 'users.id')
                 //DB::raw("count(upvotes.forum_id) as upvotecount")
                 /*->selectRaw('count(upvotes.forum_id) as upvotecount')
                 ->selectRaw('count(comments.forum_id) as commentcount') */
                 ->select('forums.forum_id', 'forums.topic', 'forums.content', 'forums.category_id', 'forums.posted_by', 'forums.created_at', 'forums.commentcount', 'forums.upvotecount', 'forums.no_views', 'users.id', 'users.profile_pic', 'users.first_name', 'users.last_name', 'users.user_type')
                 ->groupBy('forums.forum_id')
                 ->orderBy('created_at', 'DESC')
                 ->get();
                 
                 //dd($forum);
                 
                 /*query = DB::table('category_issue')
                    ->select(array('issues.*', DB::raw('COUNT(issue_subscriptions.issue_id) as followers')))
                    ->where('category_id', '=', 1)
                    ->join('issues', 'category_issue.issue_id', '=', 'issues.id')
                    ->left_join('issue_subscriptions', 'issues.id', '=', 'issue_subscriptions.issue_id')
                    ->group_by('issues.id')
                    ->order_by('followers', 'desc')
                    ->get();*/
                 
                 //working
                 /*->select('forums.forum_id', 'forums.topic', 'forums.content', 'forums.category_id', 'forums.posted_by', 'forums.created_at', 'users.id', 'users.profile_pic', 'users.first_name', 'users.last_name')
                 // DB::raw("count(upvotes.forum_id) as count")
                 //->selectRaw('count(comments.forum_id) as commentCount')
                 ->orderBy('created_at', 'DESC')
                 //->groupBy('forums.forum_id');
                 ->get();*/

        if(!empty($forum)){
            $status=TRUE;
            return response()->json(['status'=>$status, 'forum'=>$forum]);
        }
        else{
            $status=FALSE;
            $msg="No threads have been started yet";
            return response()->json(['status'=>$status, 'msg'=>$msg]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(!empty($request->topic) && !empty($request->content))
        {
            $forum = new Forum;
            $forum->topic = $request->topic;
            $forum->content = $request->content;
            $forum->posted_by = Auth::guard('api')->id();
            if(!empty($request->category_id)){
            $forum->category_id = $request->category_id;}
            $forum->status = 'Active';
            $forum->save();

            $forumpost = Forum::select('forum_id', 'topic', 'content', 'category_id', 'posted_by', 'created_at')->where('forum_id',$forum->forum_id)->first();
            //$commentno = Comment::where('forum_id',$forum->forum_id)->count();

            if(count($forumpost)>0){            
            $status=TRUE;
            return response()->json(['status'=>$status, 'forum'=>$forumpost]);
            }
            else{
                $status=FALSE;
                $msg = "Error getting post";
                return response()->json(['status'=>$status, 'msg'=>$msg]);
            }
        }
        else{
                if(empty($request->topic)){
                    $msg = "Please enter a title";
                }
                if(empty($request->content)){
                    $msg = "Please enter content for your thread";
                }
                
                if(!empty($msg))
                {
                    $status=FALSE;
                    return response()->json(['status'=>$status, 'msg'=>$msg]);
                }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Forum  $forum
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // 
        /*$forum = DB::table('forums')
                        ->join('comments', 'forums.forum_id', '=', 'comments.forum_id')
                        ->select('forums.*', 'comments.*') 
                        ->get();*/
        $forum = Forum::find($id);
        $forum->no_views = $forum->no_views + 1;
        $forum->save();
                        
        $forum = Forum::select('forum_id', 'topic', 'content', 'posted_by', 'category_id', 'no_views', 'posted_by', 'created_at')->where('forum_id', $id)->where('status','Active')->get();                
        $comments = Comment::join('users', 'comments.posted_by', '=', 'users.id')
                    ->select('comments.id', 'comments.comment', 'comments.forum_id', 'comments.reply_id', 'comments.posted_by', 'comments.created_at', 'users.id', 'users.profile_pic', 'users.first_name', 'users.last_name', 'users.user_type')
                    ->where('forum_id', $id)
                    ->orderBy('created_at', 'DESC')
                    ->get();

        $user_id = Auth::guard('api')->id();            
        $checkupvotes =  Upvote::Where('forum_id', $id)->where('user_id', $user_id)->get();  

        if(count($checkupvotes)>0){ 
            $upvoted = TRUE;
        }else{
            $upvoted = FALSE;
        }

        $upvotes = Upvote::select('id', 'forum_id', 'user_id')->where('forum_id', $id)->count();

        if(count($forum)>0)
        {
            $status = TRUE;
            return response()->json(['status'=>$status, 'forum'=>$forum, 'comments'=>$comments, 'upvotes'=>$upvotes, 'checkupvote'=>$upvoted]);
        }
        else{
            $status = FALSE;
            $msg = "Thread not found";
            return response()->json(['status'=>$status, 'msg'=>$msg]);
        } 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Forum  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Forum  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $forum = Forum::find($id);

        if(count($forum)>0)
        {
            if(!empty($request->topic))
            {
                $forum->topic = $request->topic;
            }
            if(!empty($request->content))
            {
                $forum->content = $request->content;
            }
            if(!empty($request->category_id)){
            $forum->category_id = $request->category_id;}
            $forum->save();

            $forumpost = Forum::select('forum_id', 'topic', 'content', 'category_id')->where('forum_id',$forum->forum_id)->first();

            if(count($forumpost)>0){            
            $status=TRUE;
            return response()->json(['status'=>$status, 'forum'=>$forumpost]);
            }
            else{
                $status=FALSE;
                $msg = "Error getting post";
                return response()->json(['status'=>$status, 'msg'=>$msg]);
            }
        }
        else{
                $status=FALSE;
                $msg = "Error getting post";
                return response()->json(['status'=>$status, 'msg'=>$msg]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Forum  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Forum::destroy($id);
        $status=TRUE;
        $msg = "Successfully deleted post";
        return response()->json(['status'=>$status, 'msg'=>$msg]);
    }

    public function getcategories()
    {
        $forum_cat = Forumcategory::all();
        if(count($forum_cat)>0)
        {
        $status=TRUE;
        return response()->json(['status'=>$status, 'forum_categories'=>$forum_cat]);
        }
        else{
        $status=FALSE;
        $msg = "No categories have been added";
        return response()->json(['status'=>$status, 'msg'=>$msg]);
        }
    }
}

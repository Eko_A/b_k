<?php

namespace App\Http\Controllers;

use App\Resorce;
use Storage;
use Illuminate\Http\Request;
use Auth;

class ResorceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $resources = Resorce::select('id','caption', 'description', 'link', 'photo', 'document', 'category_id', 'created_at')->where('status', 'ACTIVE')->orderBy('id', 'DESC')->get();
        if(count($resources)>0)
        {
            $status = TRUE;
            return response()->json(['status'=>$status, 'resources'=>$resources]);      
        }
        else{
            $status = TRUE;
            $msg = "No resources have been added yet";
            return response()->json(['status'=>$status, 'msg'=>$msg, 'resources'=>$resources]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }
    
    public function sendMessage($data,$target)
    {
            /*  
        Parameter Example
        	$data = array('post_id'=>'12345','post_title'=>'A Blog post');
        	$target = 'single tocken id or topic name';
        	or
        	$target = array('token1','token2','...'); // up to 1000 in one request
        */    
                
            //FCM api URL
            $url = 'https://fcm.googleapis.com/fcm/send';
            //api_key available in Firebase Console -> Project Settings -> CLOUD MESSAGING -> Server key
            $server_key = 'AAAAaTqVlwo:APA91bHpyQSWZourOuKXavpFnoSNOcfUHxxAHzy-bdGj85GUpdsjgJTWuR6Tk4u1Z6Z1p0mptBj_CIxdKkJInsKu0e2Rlo7lrBHii2f67lJ6JiCoMbU-Q-rbnrG3qslmNJRYn2DEPUv3';
            
            //return $server_key;
            			
            $fields = array();
            //$fields['data'] = $data;
            $notif = array();
            $notif['title'] = "New Resource";
            $notif['body'] = $data;
            $notif['content_available'] = true;
            $notif['priority'] = "high";
            $fields['notification'] = $notif;
            
            if(is_array($target)){
            	$fields['registration_ids'] = $target;
            }else{
            	$fields['to'] = $target;
            }
            
            //header with content_type api key
            $headers = array(
            	'Content-Type:application/json',
              'Authorization:key='.$server_key
            );
            			
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
            $result = curl_exec($ch);
            /*if ($result === FALSE) {
            	die('FCM Send Error: ' . curl_error($ch));
            }*/
            curl_close($ch);
            return $result;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $resource = new Resorce;
        if(!empty($request->caption))
        {
            $resource->caption = $request->caption;
        }
        else
        {
            $status = FALSE;
            $msg = "Please provide a caption";
            return response()->json(['status'=>$status, 'msg'=>$msg]);
        }
        
        if(!empty($request->description))
        {
            $resource->description = $request->description;
        }
        else
        {
            $status = FALSE;
            $msg = "Please provide a description";
            return response()->json(['status'=>$status, 'msg'=>$msg]);
        }
        
        if(!empty($request->link))
        {
            $resource->link = $request->link;
        }
        if(!empty($request->photo))
        {
           $imagename = time().'.'.$request->photo->getClientOriginalExtension();
           $request->photo->move(public_path('storage/resources'), $imagename);
           $resource->photo = $imagename;
        }
        if(!empty($request->document))
        {
           $filename = time().'.'.$request->document->getClientOriginalExtension();
           $request->document->move(public_path('storage/resources'), $filename);

            $resource->document = $filename;
        }
        if(!empty($request->category_id))
        {
            $resource->category_id = $request->category_id;
        }
        $resource->user_id = Auth::guard('api')->id();
        $resource->save();
        
        
        $data = $request->caption;
	    $target = '/topics/general_information';
        $res = $this->sendMessage($data, $target);
        if($res === FALSE)
        {
            $status = TRUE;
            $msg = "Successfully posted resource";
            return response()->json(['status'=>$status, 'msg'=>$msg]);
        }    

        $status = TRUE;
        $msg = "Successfully posted resource";
        return response()->json(['status'=>$status, 'msg'=>$msg]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $resource = Resorce::select('id', 'caption', 'link', 'photo', 'document', 'category_id', 'created_at')->where('status', 'ACTIVE')->where('id', $id)->first();
        if(count($resource)>0)
        {
            $status = TRUE;
            return response()->json(['status'=>$status, 'resource'=>$resource]);
        }
        else{
            $status = FALSE;
            $msg = "Resource not found";
            return response()->json(['status'=>$status, 'resource'=>$resource]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        
        if(!empty($id)){
            
        $resource = Resorce::find($id);
        //dd($resource);
        if(!empty($request->caption))
        {
            $resource->caption = $request->caption;
        }
        
        if(!empty($request->description))
        {
            $resource->description = $request->description;
        }
    
        if(!empty($request->link))
        {
            $resource->link = $request->link;
        }
        if(!empty($request->photo))
        {
           $imagename = time().'.'.$request->photo->getClientOriginalExtension();
           $request->photo->move(public_path('storage/resources'), $imagename);
           $resource->photo = $imagename;
        }
        if(!empty($request->document))
        {
           $filename = time().'.'.$request->document->getClientOriginalExtension();
           $request->document->move(public_path('storage/resources'), $filename);

            $resource->document = $filename;
        }
        if(!empty($request->category_id))
        {
            $resource->category_id = $request->category_id;
        }
        
        $resource->save();

        $status = TRUE;
        $msg = "Successfully updated resource";
        return response()->json(['status'=>$status, 'msg'=>$msg]);
        
        }
        else
        {
            $status = FALSE;
            $msg = "Please provide resource ID";
            return response()->json(['status'=>$status, 'msg'=>$msg]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Resorce::destroy($id);
        $status = TRUE;
        $msg = "Successfully deleted resource";
        return response()->json(['status'=>$status, 'msg'=>$msg]);
    }
}

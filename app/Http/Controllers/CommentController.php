<?php

namespace App\Http\Controllers;

use App\Comment;
use App\User;
use App\Forum;
use Illuminate\Http\Request;
use Auth;
use DB;

class CommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(!empty($request->comment) && !empty($request->forum_id)){
        $comment = new Comment;
        $comment->comment = $request->comment;
        $comment->forum_id = $request->forum_id;
        if(!empty($request->reply_id)){
        $comment->reply_id = $request->reply_id;
        }
        $comment->posted_by = Auth::guard('api')->id();
        $comment->save();
        
        $forum = Forum::find($request->forum_id);
        $forum->commentcount = $forum->commentcount + 1;
        $forum->save();

        $commentpost = Comment::join('users', 'comments.posted_by', '=', 'users.id')
                    ->select('comments.id', 'comments.comment', 'comments.forum_id', 'comments.reply_id', 'comments.posted_by', 'comments.created_at', 'users.id', 'users.profile_pic', 'users.first_name', 'users.last_name')
                    ->where('comments.id', $comment->id)
                    ->get();

            if(count($commentpost)>0){            
            $status=TRUE;
                return response()->json(['status'=>$status, 'comment'=>$commentpost]);
            }
            else{
                $status=FALSE;
                $msg = "Error getting comment";
                return response()->json(['status'=>$status, 'msg'=>$msg]);
            }    
        }
        else{
             if(empty($request->comment))
             {
                $status=FALSE;
                $msg = "Please enter a comment";
                return response()->json(['status'=>$status, 'msg'=>$msg]);
             }

            if(empty($request->forum_id))
             {
                $status=FALSE;
                $msg = "Please enter the forum id";
                return response()->json(['status'=>$status, 'msg'=>$msg]);
             }             
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Comment  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
 
     * Show the form for editing the specified resource.
     *
     * @param  \App\Comment  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

   /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Comment  $id
     * @return \Illuminate\Http\Response
    */
     
    public function update(Request $request, $id)
    {
        //
        if(!empty($id)){
        $comment = Comment::find($id);
        if(!empty($request->comment)){
        $comment->comment = $request->comment;
        }
        $comment->save();

        $commentpost = Comment::join('users', 'comments.posted_by', '=', 'users.id')
                    ->select('comments.id', 'comments.comment', 'comments.forum_id', 'comments.reply_id', 'comments.posted_by', 'comments.created_at', 'users.id', 'users.profile_pic', 'users.first_name', 'users.last_name')
                    ->where('comments.id', $id)
                    ->get();

            if(count($commentpost)>0){            
            $status=TRUE;
                return response()->json(['status'=>$status, 'comment'=>$commentpost]);
            }
            else{
                $status=FALSE;
                $msg = "Error getting comment";
                return response()->json(['status'=>$status, 'msg'=>$msg]);
            }    
        }
        else
        {
                $status=FALSE;
                $msg = "Please enter the comment id";
                return response()->json(['status'=>$status, 'msg'=>$msg]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Comment  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $comment = Comment::find($id);
            
        $forum = Forum::find($comment->forum_id);
        $forum->commentcount = $forum->commentcount - 1;
        $forum->save();
        
        Comment::destroy($id);
        $status=TRUE;
        $msg = "Successfully deleted comment";
        return response()->json(['status'=>$status, 'msg'=>$msg]);
    }
}

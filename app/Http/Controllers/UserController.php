<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
//use Illuminate\Auth\TokenGuard::attempt;
use App\User;
use App\Forum;
use App\Sme;
use Auth;
use Storage;
use DB;
use Mail;

class UserController extends Controller
{

    public function login(Request $request)
    { 
        $email = $request->email;
        $password = $request->password;

        if(!empty($email) && !empty($password))
        {
            $user = User::where('email', $request->email)
                  ->where('password',md5($request->password))
                  ->first();

            if(Auth::attempt(array('email' => $email, 'password' => $password)))
            {
                $status = TRUE;
                $user = User::select('id','profile_pic', 'first_name', 'middle_name', 'last_name', 'about', 'email', 'api_token', 'user_type', 'position', 'sme_id', 'status')->where('email', $email)->first();
                
                if($user->status == "BLOCKED")
                {
                    $status = FALSE;
                    $msg = "Your account has been blocked, contact support";
                    return response()->json(['status'=>$status, 'msg'=>$msg]);
                }
                
                return response()->json(['user'=>$user, 'status'=>$status]);
            }
            else{
                $status = FALSE;
                $msg = "Invalid credentials";
                return response()->json(['status'=>$status, 'msg'=>$msg]);
            }
        }
        else if(empty($email)){
            $status = FALSE;
            $msg = "Kindly enter your email address";
            return response()->json(['status'=>$status, 'msg'=>$msg]);
        }
        else if(empty($password))
        {
            $status = FALSE;
            $msg = "Kindly enter your password";
            return response()->json(['status'=>$status, 'msg'=>$msg]);
        }
    }

    public function register(Request $request)
    {
        if(!empty($request->first_name) && !empty($request->middle_name) && !empty($request->last_name) && !empty($request->email) && !empty($request->password)){
            $first_name = $request->first_name;
            $middle_name = $request->middle_name;
            $last_name = $request->last_name;
            $email = $request->email;
            $user_type = $request->user_type;
            $api_token = str_random(60);
            $password = bcrypt($request->password); 
            //$password = m($request->password); 

            $checkemail = User::where('email', $email)->first();
            if(count($checkemail)>0)
            {
                $msg = "This email is already in use"; 
            }
            else{
                $user = new User;

                if(empty($request->profile_pic)){
                $user->profile_pic = 'default.jpg';
                }
                else{
                    
                    $imagename = time().'.'.$request->profile_pic->getClientOriginalExtension();
                    $request->profile_pic->move(public_path('storage/profile_pic'), $imagename);
                    $user->profile_pic = $imagename;
                    
                    /*$filename = "Helloo.jpg";
                    Storage::disk('profile_pic')->put($filename, $request->profile_pic);
                    $user->profile_pic = $filename;*/
                }
                $user->first_name = $first_name;
                $user->middle_name = $middle_name;
                $user->last_name = $last_name;
                $user->email = $email;
                $user->user_type = $user_type;
                $user->api_token = $api_token;
                $user->password = $password;
                $user->save();

                $user = User::select('id','profile_pic', 'first_name', 'middle_name', 'last_name', 'about', 'email', 'api_token', 'user_type', 'position', 'sme_id')->where('email', $email)->first();
                
                if(count($user)>0){ 
                    
                    $data = array(
                        'name' => $user->first_name,
                    );
                    try{
                        Mail::send('emails.welcome', ['name' => $first_name], function ($message) use($email) {
                    
                            $message->from('do-not-reply@black-knights.net', 'Black Knights');
                    
                            $message->to($email)->subject('Welcome Mail');
                    
                        });
                    }
                    catch(\Exception $e){
                        $status = TRUE;
                        return response()->json(['status'=>$status, 'user'=>$user]);
                    }
                    
                    $status = TRUE;
                    return response()->json(['status'=>$status, 'user'=>$user]);
                }
                else{
                    $status = FALSE;
                    $msg = "User not found";
                    return response()->json(['status'=>$status, 'msg'=>$msg]);
                }                
            }
        }
        else
        {
            if(empty($request->first_name))
            {
                $msg = "Kindly enter your first name"; 
            }
            if(empty($request->middle_name))
            {
                $msg = "Kindly enter your middle name"; 
            }
            if(empty($request->last_name))
            {
                $msg = "Kindly enter your last name"; 
            }
            if(empty($request->email))
            {
                $msg = "Kindly enter your email address"; 
            }
            if(empty($request->user_type))
            {
                $msg = "Kindly enter your profile type"; 
            }
            if(empty($request->password))
            {
                $msg = "Kindly enter your password"; 
            }
        }

        if(!empty($msg))
        {
            $status = FALSE;
            return response()->json(['status'=>$status, 'msg'=>$msg]);
        }

    }
    
    //send password reset mail
    public function passwordreset(Request $request)
    {
        //return "Hello";
        $email  = $request->email;
        
        if(!empty($email))
        {
            $user = User::select('id', 'first_name','email')->where('email', $email)->first();
            
            $first_name = $user->first_name; 
            
            DB::table('password_resets')->insert(
                array('user_id' => "$user->id")
            );
            
            $hashy = "GH39IJ393UN3440NCNKE8740JJHEMQGH39IJ393UN3440NCNKE8740JJHEMQ". $user->id;
            
            $link = "https://ekoadetolani.com/black_knights/resetpassword/$hashy";
            
            if(count($user)>0)
            {
                 $data = array(
                        'name' => "$user->name",
                    );
                    
                    try{
                        Mail::send('emails.passwordreset', ['name' => $first_name, 'link'=>$link], function ($message) use($email) {
                
                        $message->from('do-not-reply@black-knights.net', 'Black Knights');
                
                        $message->to($email)->subject('Password Reset');
                
                    });
                    }
                    catch(\Exception $e){
                        $status = TRUE;
                        $msg = "A password reset email has been sent to your mail";
                        return response()->json(['status'=>$status, 'msg'=>$msg]);
                    }
                
                    $status = TRUE;
                    $msg = "A password reset email has been sent to your mail";
                    return response()->json(['status'=>$status, 'msg'=>$msg]); 
            }
            else
            {
                $status = FALSE;
                $msg = "This email address doesn't exist";
                return response()->json(['status'=>$status, 'msg'=>$msg]);
            }
        }
        else
        {
                $status = FALSE;
                $msg = "Please provide a valid email address";
                return response()->json(['status'=>$status, 'msg'=>$msg]);
        }
    }
    
    //displays page
    public function resetpassword()
    {
        return view('blackknights.resetpasword');
    }
    
    
    public function updatepassword(Request $request)
    {
        if(empty($request->link))
        {
            return "This link has expired";
        }
        $id = str_replace("/black_knights/resetpassword/GH39IJ393UN3440NCNKE8740JJHEMQGH39IJ393UN3440NCNKE8740JJHEMQ", "", "$request->link");

        //$id = substr("$request->link", -1);
        $check = DB::table('password_resets')->select('id', 'isUsed')->where('user_id', $id)->latest('id')->first();
        
        
        if($check->isUsed == 1){
            return "This link has expired";
        }
        else
        {
            $user = User::find($id);
            $user->password = bcrypt($request->password);
            $user->save();
            
            DB::table('password_resets')
            ->where('id', $check->id)
            ->update(['isUsed' => 1]);
            
            $msg = "Password has been updated";
            return $msg;
        }
    }
    
    //change password -- in app
    public function changepassword(Request $request)
    {
        if(empty($request->current_password))
        {
            $status = FALSE;
            $msg = "Please provide your current password";
            return response()->json(['status'=>$status, 'msg'=>$msg]);
        }
        
        if(empty($request->new_password))
        {
            $status = FALSE;
            $msg = "Please provide your new password";
            return response()->json(['status'=>$status, 'msg'=>$msg]);
        }
        
        if(empty(Auth::guard('api')->id()))
        {
            $status = FALSE;
            $msg = "Please provide authentication";
            return response()->json(['status'=>$status, 'msg'=>$msg]);
        }
        
        $user_id = Auth::guard('api')->id();
        
        $user = User::find($user_id);
        $email = $user->email;
        $password = $request->current_password;
        
        if(Auth::attempt(array('email' => $email, 'password' => $password)))
            {
                $user->password = bcrypt($request->new_password);
                $user->save();
                
                $status = TRUE;
                $msg = "Successfully changed password";
                return response()->json(['status'=>$status, 'msg'=>$msg]);
            }
            else
            {
                $status = FALSE;
                $msg = "Password provided is wrong";
                return response()->json(['status'=>$status, 'msg'=>$msg]);
            }
    }

    public function search(Request $request)
    {

    }

    public function discover(Request $request)
    {
        $user_id = Auth::guard('api')->id();
        if(!empty($request->search_query)){
            $search_query = $request->search_query;

            $words = explode(' ', $search_query);
            $userresult = User::select('id','profile_pic','first_name', 'middle_name', 'last_name', 'email', 'user_type', 'position', 'sme_id')->where(function ($query) use($words) {
                                foreach($words as $word) { 
                                    $query->orWhere('first_name', 'like', '%' . $word . '%')->orwhere('middle_name', 'like', '%' . $word . '%')->orwhere('last_name', 'like', '%' . $word . '%');
                                }
                                })
                                ->where('id', '!=', $user_id)
                                ->get();

            $smeresult = Sme::select('id','profile_pic', 'business_name', 'short_bio', 'industry', 'badge_id')->where(function ($query) use($words) {
                                foreach($words as $word) {
                                    $query->orWhere('business_name', 'like', '%' . $word . '%');
                                    }
                                    })->get();

            if(count($smeresult)>0 || count($userresult)>0)
            {
                $status = TRUE;
                return response()->json(['status'=>$status, 'smes'=>$smeresult, 'users'=>$userresult]);
            }
            else{
                $status = TRUE;
                return response()->json(['status'=>$status, 'smes'=>$smeresult, 'users'=>$userresult]);
                /*$msg = "No result(s) found for $search_query";
                return response()->json(['status'=>$status, 'msg'=>$msg]);*/
            }
        }
        else{
            $status = FALSE;
            $msg = "No search query";

            return response()->json(['status'=>$status, 'msg'=>$msg]);
        }
    }

    public function getuser($id)
    {
        if(!empty($id)){
        $user = User::select('profile_pic', 'first_name', 'middle_name', 'last_name', 'email', 'api_token', 'user_type', 'position', 'sme_id')->where('id', $id)->first();
        }
        else{
            $status = FALSE;
            $msg = "Id not available";
            return response()->json(['status'=>$status, 'msg'=>$msg]);   
        }

        if(!empty($user))
        {
            $status = TRUE;
            return response()->json(['status'=>$status, 'user'=>$user]);
        }
        else{
            $status = FALSE;
            $msg = "User not found";
            return response()->json(['status'=>$status, 'msg'=>$msg]); 
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
       /* return User::create([
            'first_name' => $request['first_name'],
            'middle_name' => $request['middle_name'],
            'last_name' => $request['last_name'],
            'email' => $request['email'],
            'api_token' => str_random(60),
            'password' => bcrypt($request['password']),
        ]);*/
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $user = User::select('first_name', 'middle_name', 'last_name', 'email', 'api_token', 'user_type', 'position', 'sme_id')->where('id', $id)->first();
       if(count($user)>0)
       {
            $status=TRUE;
            return response()->json(['status'=>$status, 'user'=>$user]);
       }
       else{
           $status = FALSE;
           $msg = "User not found";
           return response()->json(['status'=>$status, 'msg'=>$msg]);
       }
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        //$user = User::select('first_name', 'middle_name', 'last_name', 'email', 'api_token', 'user_type')->where('id', $id)->first();  
        $user = User::find($id);
        if(count($user)>0)
        {
            if(!empty($request->profile_pic))
            {
               $imagename = time().'.'.$request->profile_pic->getClientOriginalExtension();
               $request->profile_pic->move(public_path('storage/profile_pic'), $imagename);
               $user->profile_pic = $imagename;
            }
            
            if(!empty($request->first_name)){
                $user->first_name = $request->first_name;
            }
            if(!empty($request->middle_name)){
                $user->middle_name = $request->middle_name;
            }
            if(!empty($request->last_name)){
                $user->last_name = $request->last_name;
            }
            if(!empty($request->about)){
                $user->about = $request->about;
            }
            if(!empty($request->email)){
                $useremail = User::where('email', $request->email)->where('id','!=', $id)->first();
                if(count($useremail>0))
                {
                    $status = FALSE;
                    $msg = "Email already in use";
                    return response()->json(['status'=>$status, 'msg'=>$msg]);
                }
                else{
                    $user->email = $request->email;
                }
            }
            $user->save();
            
            $status = TRUE;
            $msg = "Successfully updated details";
            
            $user = User::select('profile_pic', 'first_name', 'middle_name', 'last_name', 'email')->where('id', $id)->get();
            
            return response()->json(['status'=>$status, 'msg'=>$msg, 'user'=>$user]);
        }
    }

    public function entrepreneurprofile($id)
    {
        //dd("Hello");
    /* $sme = DB::table('smes')
        ->join('badge_categories', 'smes.badge_id', '=', 'badge_categories.id')
        ->select('smes.id','smes.profile_pic', 'smes.business_name', 'smes.short_bio', 'smes.industry', 'badge_categories.badge_name')
        ->where('smes.id',$id)
        ->get();*/
        
        $user = User::select('id','profile_pic', 'first_name', 'middle_name', 'last_name', 'email', 'about', 'user_type', 'sme_id', 'position')
                ->where('id', $id)
                ->first();
       
                        /*->join('badge_categories', 'smes.badge_id', '=', 'badge_categories.id')
        ->select('smes.id','smes.profile_pic', 'smes.business_name', 'smes.short_bio', 'smes.industry', 'badge_categories.badge_name')*/
               //dd($user->sme_id);
        $sme_details = Sme::join('startup_categories', 'smes.industry', '=', 'startup_categories.categories_id')
                        ->join('badge_categories', 'smes.badge_id', '=', 'badge_categories.id')
                        ->select('smes.id','smes.profile_pic', 'smes.business_name', 'smes.short_bio', 'smes.industry','smes.user_id', 'startup_categories.categories_name','badge_categories.id as badgeId', 'badge_categories.badge_name')
                        ->where('smes.id', $user->sme_id)
                        ->first();      

        //, 'startup_categories.categories_name', 'badge_categories.badge_name'
        
        
        $forum = Forum::select('forum_id', 'topic', 'content', 'commentcount', 'upvotecount', 'no_views', 'created_at')
        ->where('posted_by', $id)
        ->orderBY('created_at', 'desc')
        ->get();
        
        if(count($user)>0 )
        {
            //$sme = SME::select('id','business_name')->where('smes.id', $user->sme_id)->get();
            
            $status = TRUE;
            return response()->json(['status'=>$status, 'user'=>$user, 'forum'=>$forum, 'sme_details'=>$sme_details]);
        }
        else
        {
            $status = FALSE;
            $msg = "User not found";
            return response()->json(['status'=>$status, 'msg'=>$msg]);
        }
    }
    
    public function investorprofile($id)
    {
        $user = User::select('profile_pic', 'first_name', 'middle_name', 'last_name', 'email', 'user_type', 'position', 'sme_id')->where('id', $id)->get();
        
        $forum = Forum::select('forum_id', 'topic', 'content', 'commentcount', 'upvotecount', 'no_views', 'created_at')->where('posted_by', $id)->orderBY('created_at', 'desc')->get();
        
        if(count($user)>0 )
        {
            $status = TRUE;
            return response()->json(['status'=>$status, 'user'=>$user, 'forum'=>$forum]);
        }
        else
        {
            $status = FALSE;
            $msg = "User not found";
            return response()->json(['status'=>$status, 'msg'=>$msg]);
        }
    }
    
    public function smeprofile($id)
    {
        $sme = Sme::join('badge_categories', 'smes.badge_id', '=', 'badge_categories.id')
        ->join('startup_categories', 'smes.industry', '=', 'startup_categories.categories_id')
        ->select('smes.id','smes.profile_pic', 'smes.business_name', 'smes.short_bio', 'smes.investor_id', 'smes.industry', 'smes.stage', 'smes.location', 'smes.year_founded', 'smes.no_of_employees', 'smes.requirement', 'smes.website', 'smes.pitch_deck', 'smes.user_id', 'smes.badge_id', 'badge_categories.badge_name', 'startup_categories.categories_name')
        ->where('smes.id',$id)
        ->first();
        
        $team = User::select('id', 'first_name', 'last_name', 'position', 'sme_id', 'user_type')->where('sme_id', $id)->get();
        
        
        if(count($sme)>0 )
        {
            $status = TRUE;
            return response()->json(['status'=>$status, 'sme'=>$sme, 'team'=>$team]);
        }
        else
        {
            $status = FALSE;
            $msg = "SME not found";
            return response()->json(['status'=>$status, 'msg'=>$msg]);
        }
    }
    
    public function getinvestors()
    {
        $id = Auth::guard('api')->id();
        $users = User::select('id','profile_pic', 'first_name', 'middle_name', 'last_name', 'email', 'about', 'user_type')
                ->where('user_type', 'investor')
                ->where('id', '!=', $id)
                ->get();
                
        if(count($users)>0)
        {
            $status = TRUE;
            return response()->json(['status'=>$status, 'users'=>$users]);
        }
        else
        {
            $status = FALSE;
            $msg = "No Investors found";
            return response()->json(['status'=>$status, 'msg'=>$msg]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $forum = Forum::find($id);
        if(count($forum)>0)
        {
            Forum::destroy($id);
            $status = TRUE;
            return response()->json(['status'=>$status]);
        }
        else{
            $status = FALSE;
            $msg = "Error thread not found";
            return response()->json(['status'=>$status, 'msg'=>$msg]);
        }
        
    }
    
    public function setdeviceid(Request $request)
    {
        $user_id = $request->user_id;
        if(empty($user_id))
        {
            $status = FALSE;
            $msg = "Please provide user id";
            return response()->json(['status'=>$status, 'msg'=>$msg]);
        }
        else
        {
            $user = User::find($user_id);
            if(count($user)>0)
            {
                    if(!empty($request->device_id)){
                    $user->device_id = $request->device_id;
                    }
                    else
                    {
                        $status = FALSE;
                        $msg = "Please provide device id";
                        return response()->json(['status'=>$status, 'msg'=>$msg]);
                    }
                $user->save();
                
                $status = TRUE;
                $msg = "Successful";
                return response()->json(['status'=>$status, 'msg'=>$msg]);
            }
        }
    }
    
    public function unsetdeviceid(Request $request)
    {
        $user_id = $request->user_id;
        if(empty($user_id))
        {
            $status = FALSE;
            $msg = "Please provide user id";
            return response()->json(['status'=>$status, 'msg'=>$msg]);
        }
        else
        {
            $user = User::find($user_id);
            if(count($user)>0)
            {
                $user->device_id = null;
                $user->save();
                
                $status = TRUE;
                $msg = "Successful";
                return response()->json(['status'=>$status, 'msg'=>$msg]);
            }
        }
    }

}

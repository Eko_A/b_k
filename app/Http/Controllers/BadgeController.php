<?php

namespace App\Http\Controllers;

use App\Badge;
use App\Badgecategory;
use App\Sme;
use Illuminate\Http\Request;
use DB;
use Auth;

class BadgeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function getbadges()
    {
        $badges = DB::table('badge_categories')->select('id', 'badge_photo', 'badge_name', 'description')->get();
        if(count($badges)>0){
            $status = TRUE;
            return response()->json(['status'=>$status, 'badges'=>$badges]);
        }
        else{
            $status = FALSE;
            $msg = "No badges have been created yet";
            return response()->json(['status'=>$status, 'msg'=>$msg]);
        }
    }
    
    public function getbadgesbyinvestorid($investor_id)
    {
        $badges = Badge::select('id', 'badge_id', 'investor_id', 'milestone', 'description')->where('investor_id', $investor_id)->get();
        if(count($badges)>0){
            $status = TRUE;
            return response()->json(['status'=>$status, 'badges'=>$badges]);
        }
        else{
            $status = FALSE;
            $msg = "No badges have been created yet";
            return response()->json(['status'=>$status, 'msg'=>$msg]);
        }
    }
    
    public function getbadgesbyid($badge_id, $sme_id)
    {
        if(empty($badge_id))
        {
            $status = FALSE;
            $msg = "Please provide badge id";
            return response()->json(['status'=>$status, 'msg'=>$msg]);
        }
        
        if(empty($sme_id))
        {
            $status = FALSE;
            $msg = "Please provide sme id";
            return response()->json(['status'=>$status, 'msg'=>$msg]);
        }
        
        $badge = Badgecategory::select('id', 'badge_photo', 'badge_name', 'description')->where('id', $badge_id)->get();
        
        $sme = Sme::find($sme_id);
        if(count($sme)>0)
        {
            if($sme->investor_id != null)
            {
                $milestones = Badge::select('badge_id', 'investor_id', 'milestone', 'description')->where('badge_id', $badge_id)->where('investor_id', $sme->investor_id)->get();
                if(count($milestones)>0)
                {
                    $status = TRUE;
                    return response()->json(['status'=>$status, 'badge'=>$badge, 'milestones'=>$milestones]);
                }
                else
                {
                    $milestones = Badge::select('badge_id', 'investor_id', 'milestone', 'description')->where('badge_id', $badge_id)->where('investor_id', 0)->get();
                    
                    if(count($milestones)>0)
                    {
                    $status = TRUE;
                    return response()->json(['status'=>$status, 'badge'=>$badge, 'milestones'=>$milestones]);
                    }
                    else
                    {
                        $status = FALSE;
                        $msg = "Milestones not available";
                        return response()->json(['status'=>$status, 'msg'=>$msg]);
                    }
                }
            }
            else{
                $milestones = Badge::select('badge_id', 'investor_id', 'milestone', 'description')->where('badge_id', $badge_id)->where('investor_id', 0)->get();
                
                if(count($milestones)>0)
                {
                $status = TRUE;
                return response()->json(['status'=>$status, 'badge'=>$badge, 'milestones'=>$milestones]);
                }
                else
                {
                    $status = FALSE;
                    $msg = "Milestones not available";
                    return response()->json(['status'=>$status, 'msg'=>$msg]);
                }
            }
        }
        else
        {
            $status = FALSE;
            $msg = "SME not found";
            return response()->json(['status'=>$status, 'msg'=>$msg]);
        }
        
        /*if(Count($badge)>0)
        {
            $status = TRUE;
            return response()->json(['status'=>$status, 'badge'=>$badge]);
        }
        else
        {
            $status = FALSE;
            $msg = "Badge not found";
            return response()->json(['status'=>$status, 'msg'=>$msg]);
        }*/
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        if(!empty($request->badge_id) && !empty($request->investor_id) && !empty($request->milestone) && !empty($request->description))
        {
            $badge = new badge;
            $badge->badge_id = $request->badge_id;
            $badge->investor_id = $request->investor_id;
            $badge->milestone = $request->milestone;
            $badge->description = $request->description;
            $badge->save();

            $status = TRUE;
            $msg = "Successfully added milestone";
            return response()->json(['status'=>$status, 'msg'=>$msg]);
        }
        else{
            if(empty($request->badge_id))
            {
                $msg = "No badge id provided";
            }
            
            if(empty($request->investor_id))
            {
                $msg = "No investor id provided";
            }

            if(empty($request->milestone))
            {
                $msg = "No milestone provided";
            }

            if(empty($request->description))
            {
                $msg = "No description provided";
            }

            if(!empty($msg))
            {
                $status =FALSE;
                return response()->json(['status'=>$status, 'msg'=>$msg]);
            }
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Badge  $badge
     * @return \Illuminate\Http\Response
     */
    public function show($badge_id, $investor_id)
    {
        //
        $milestones = Badge::select('badge_id', 'investor_id', 'milestone', 'description')->where('badge_id', $badge_id)->where('investor_id', $investor_id)->get();
        if(count($badge)>0)
        {
            $status = TRUE;
            return response()->json(['status'=>$status, 'milestones'=>$milestones]);
        }
        else{
            $milestones = Badge::select('badge_id', 'investor_id', 'milestone', 'description')->where('badge_id', $badge_id)->where('investor_id', 0)->get();
            
            $status = TRUE;
            return response()->json(['status'=>$status, 'milestones'=>$milestones]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Badge  $badge
     * @return \Illuminate\Http\Response
     */
    public function edit(Badge $badge)
    {
        //
    }
    
    public function updatebadge(Request $request, $sme_id)
    {
        $sme = Sme::find($sme_id);
        $badge_id = $request->badge_id;
        $sme->save();
        
        $status = TRUE;
        $msg = "Successfully updated badge";
        return response()->json(['status'=>$status, 'msg'=>$msg]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Badge  $badge
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
            $badge = Badge::find($id);
            if(!empty($request->milestone)){
            $badge->milestone = $request->milestone;}
            if(!empty($request->description)){
            $badge->description = $request->description;}
            $badge->save();

            $status = TRUE;
            $msg = "Successfully edited badge milestone";
            return response()->json(['status'=>$status, 'msg'=>$msg]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Badge  $badge
     * @return \Illuminate\Http\Response
     */
    public function destroy(Badge $badge)
    {
        //
    }
    
}

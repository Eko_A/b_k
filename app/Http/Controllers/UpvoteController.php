<?php

namespace App\Http\Controllers;

use App\Upvote;
use App\Forum;
use Illuminate\Http\Request;
use Auth;

class UpvoteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

    }
    
    public function sendMessage($data,$target)
    {
        /*  
    Parameter Example
    	$data = array('post_id'=>'12345','post_title'=>'A Blog post');
    	$target = 'single tocken id or topic name';
    	or
    	$target = array('token1','token2','...'); // up to 1000 in one request
    */    
            
        //FCM api URL
        $url = 'https://fcm.googleapis.com/fcm/send';
        //api_key available in Firebase Console -> Project Settings -> CLOUD MESSAGING -> Server key
        $server_key = 'AAAAaTqVlwo:APA91bHpyQSWZourOuKXavpFnoSNOcfUHxxAHzy-bdGj85GUpdsjgJTWuR6Tk4u1Z6Z1p0mptBj_CIxdKkJInsKu0e2Rlo7lrBHii2f67lJ6JiCoMbU-Q-rbnrG3qslmNJRYn2DEPUv3';
        
        //return $server_key;
        			
        $fields = array();
        //$fields['data'] = $data;
        $notif = array();
        $notif['title'] = "New upvote";
        $notif['body'] = $data;
        $notif['content_available'] = true;
        $notif['priority'] = "high";
        $fields['notification'] = $notif;
        
        if(is_array($target)){
        	$fields['registration_ids'] = $target;
        }else{
        	$fields['to'] = $target;
        }
        
        //header with content_type api key
        $headers = array(
        	'Content-Type:application/json',
          'Authorization:key='.$server_key
        );
        			
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);
        /*if ($result === FALSE) {
        	die('FCM Send Error: ' . curl_error($ch));
        }*/
        curl_close($ch);
        return $result;
    }

    public function upvote(Request $request)
    {
        if(!empty($request->forum_id)){
        $upvote = new Upvote;
        $upvote->forum_id = $request->forum_id;
        $upvote->user_id = Auth::guard('api')->id();
        $upvote->save();

        $forum = Forum::find($request->forum_id);
        $forum->upvotecount = $forum->upvotecount + 1;
        $forum->save();
        
        $user = User::find($forum->posted_by);
            if(!empty($user->device_id))
            {
                //send push notification
                $data = $forum->topic;
        	    $target = $user->device_id;
                $res = $this->sendMessage($data, $target);
                if($res === FALSE)
                {
                    $status = TRUE;
                    $msg = "Successful";
                    return response()->json(['status'=>$status, 'msg'=>$msg]);
                }
            }
        
        $countupvote = Upvote::where('forum_id', $request->forum_id)->count();
        //dd($countupvote);
        if($countupvote == 1 || $countupvote % 5 == 0)
        {
            $user = User::find($forum->posted_by);
            if(!empty($user->device_id))
            {
                //send push notification
                $data = $forum->topic;
        	    $target = $user->device_id;
                $res = $this->sendMessage($data, $target);
                if($res === FALSE)
                {
                    $status = TRUE;
                    $msg = "Successful";
                    return response()->json(['status'=>$status, 'msg'=>$msg]);
                }
            }
        }

        $status=TRUE;
        $msg = "Successfully upvoted post";
        return response()->json(['status'=>$status, 'msg'=>$msg]);
        }
        else{
        $status=FALSE;
        $msg = "Forum ID not found";
        return response()->json(['status'=>$status, 'msg'=>$msg]);
        }
    }

    public function downvote($forum_id)
    {
        $user_id = Auth::guard('api')->id();
        $upvote = Upvote::where('forum_id', $forum_id)->where('user_id', $user_id)->first();
        if(count($upvote)>0){
            Upvote::destroy($upvote->id);
            
            $forum = Forum::find($forum_id);
            
            if($forum->upvotecount > 0){
                $forum->upvotecount = $forum->upvotecount - 1;
            }
            $forum->save();
            
            $status=TRUE;
            $msg = "Successfully downvoted post";
            return response()->json(['status'=>$status, 'msg'=>$msg]);
        }
        else
        {
            $status = FALSE;
            $msg = "You haven't upvoted this post";
            return response()->json(['status'=>$status, 'msg'=>$msg]);
        }
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

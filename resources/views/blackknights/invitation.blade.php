<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
</head>
<body>
<title>Black Knights</title>
<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<!------ Include the above in your HEAD tag ---------->

 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
 
 <style>
     .form-gap {
    padding-top: 70px;
}
.gold{
    color:#D18F15;
}
 </style>
 
 <div class="form-gap"></div>
<div class="container">
	<div class="row">
		<div class="col-md-4 col-md-offset-4">
            <div class="panel panel-default">
                <?php 
                if(!empty($_GET['msg']))
                {
                    echo "<p style='color:green'>$_GET('msg')</p>";
                }
                ?>
              <div class="panel-body" style="background-color:#2C2F31">
                <div class="text-center">
                  <h3><i class="fa fa-user fa-4x gold"></i></h3>
                  <h2 class="text-center" style="color:#fff">Team Invitation</h2>
                  <p style="color:#fff" >Please enter your position below</p>
                  <div class="panel-body">
    
                    <form id="register-form" role="form" autocomplete="off" action="{{ route('invitation.position') }}" class="form" method="POST">
    
                      <div class="form-group">
                        <div class="input-group">
                          <span class="input-group-addon"><i class="glyphicon glyphicon-user color-blue"></i></span>
                          <input id="myInput" name="position" placeholder="position" class="form-control"  type="text" required>
                        </div>
                      </div>
                      
                      <div class="form-group">
                        <input style="background-color:#D18F15" name="recover-submit" class="btn btn-lg btn-primary btn-block" value="Save" type="submit">
                      </div>
                      <?php
                      $actual_link = "$_SERVER[REQUEST_URI]";
                      ?>
                      
                      <input type="hidden" class="hide" name="link" id="link" value="<?php echo $actual_link; ?>"> 
                    </form>
    
                  </div>
                </div>
              </div>
            </div>
          </div>
	</div>
</div>
</body>
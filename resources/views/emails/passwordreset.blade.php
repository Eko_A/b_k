<html>
    <head>
        <title>Black Knights</title>
    </head>
    <style>
        body{
            font-family: arial;
            color: #fff;
        }
        .wrapper{
            height: 750px;
            width: 600px;
            margin: 0 auto;
            background-color: #2c2f31;
            text-align: center;
            padding: 25px;
        }
        .textbox{
            text-align: left !important;
        }
        .footer{
            background-color: #f9fafc;
            width: 600px;
            padding: 25px;
            height: 80px;
            margin: 0 auto;
            color: #888 !important;
            font-size: 10pt;
        }
        .foot{
            color: #888;
            font-size: 10pt;
        }
    </style>
    <body>
        <div class="wrapper">
            <img src="https://ci3.googleusercontent.com/proxy/vKoReWV_olUPyZe_VZtDAPumqtU7fdAZ05QhZ13wRpTgcJ2cuKVxCDuXqmNdTTAxMck8YRSupgoxKWpPpOpkZ-ISB0wM64CuPav3vB94RainhFotTBquFDkh38_g=s0-d-e1-ft#https://ekoadetolani.com/black_knights/storage/emails/blackknightslogo.png" alt="">
            <h2>Password Reset</h2>
            <img width="80%" src="https://ekoadetolani.com/black_knights/storage/emails/passwordreset.png" alt="photo">
            <div class="textbox">
                <h3 style="color: #d18f15;">Password Reset</h3>
                <p>Hello {{$name}},</p>
                <p><span>To reset your password please click <a href="{{$link}}">here</a></span></p>
            </div>
        </div>
        <div class="footer">
            <table class="foot" style="width: 100%">
                <tr>
                    <td>
                        <p>Black Knights,<br/>Lagos, Nigeria <br/>hello@black-knights.net</p>
                    </td>
                    <td style="text-align: right;">
                        <img src="https://ci6.googleusercontent.com/proxy/YUACoyNrCAj8aCPPladwYoIZxBvKo8ajoxsv_AHmTbvL8G5yIFcZjjddqlNDyIqDFL5CBBzahEZ9i1Ca3spZNAAE41KRRavbJ3LW5NTWTyWw=s0-d-e1-ft#http://img.mailinblue.com/new_images/rnb/theme2/rnb_ico_fb.png" alt="">
                        <img src="https://ci4.googleusercontent.com/proxy/RmwF1JljhPAEUdWZYzxT9NeMtxry-zEK_J7hled4uscl4NzXXs3dnOpoXN0t8UzouVjI_d8CsW_ZyBeWUlnJGfGXr6cR2Dec5ZdtUPmi1-Es=s0-d-e1-ft#http://img.mailinblue.com/new_images/rnb/theme2/rnb_ico_tw.png" alt="">
                        <img src="https://ci5.googleusercontent.com/proxy/rIfWMb0Hz8V6f5XoNdeYrYFNFnRhmoP-U0dwsI_hnRUnp1D_cLzrMIj8Yyh7nvjJnfRuUSrYH2jOKobU9su37udjUcZgDKagoyBAloLyuGqc=s0-d-e1-ft#http://img.mailinblue.com/new_images/rnb/theme2/rnb_ico_in.png" alt="">
                        <img src="https://ci4.googleusercontent.com/proxy/kQjnxzD5ZUiq9LMnSsjdzlGT_DqEmm2g39OQH31th1_LDGcE5KhzsI-Klfc5EDQzCyGM-C_tNPjWyQRq_d8MgespGGjraqzmcx5nshCygef7=s0-d-e1-ft#http://img.mailinblue.com/new_images/rnb/theme2/rnb_ico_ig.png" alt="">
                        <img src="https://ci6.googleusercontent.com/proxy/rQdPDDtWPL8bsFsTqTX1R7TS_Ymw7SmsuPPTFQcyyk_rOkhqZ0pJ1wV8Jp_Xb-1TIQpYMgX1lfxChSk9vscLXjeZE3E6VgUE8JEkf4VX2SOs=s0-d-e1-ft#http://img.mailinblue.com/new_images/rnb/theme2/rnb_ico_yt.png" alt="">

                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="text-align: center; padding-top: 3%;">
                        &copy; 2018 Black Knights
                    </td>
                </tr>
            </table>
        </div>
    </body>
</html>